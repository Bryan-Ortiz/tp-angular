import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { PraticienComponent } from './praticien/praticien.component';
import { PatientComponent } from './patient/patient.component';
import { RendezVousComponent } from './rendez-vous/rendez-vous.component';
import { AdresseComponent } from './adresse/adresse.component';

@NgModule({
  declarations: [
    AppComponent,
    PraticienComponent,
    PatientComponent,
    RendezVousComponent,
    AdresseComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
